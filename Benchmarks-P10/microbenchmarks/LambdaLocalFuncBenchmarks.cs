﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.benchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class LambdaLocalFuncBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Func<>")]
	public static ulong Lambda() {
		ulong result = 0;
		Func<ulong> testFunc = FuncTest;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + testFunc() + i;
		}

		return result;
	}

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Func<> with closure")]
	public static ulong LambdaClosure() {
		ulong result = 0;
		Func<ulong> testFunc = delegate { return result + 10; };
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + i;
		}

		return result;
	}

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Func<> with a param")]
	public static ulong LambdaParam() {
		ulong result = 0;
		Func<ulong, ulong> testFunc = FuncTestParam;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc(result) + i;
		}

		return result;
	}

	private delegate ulong Test();

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Delegate")]
	public static ulong LambdaDelegate() {
		ulong result = 0;
		Test testFunc = FuncTest;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + result + i;
		}

		return result;
	}

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Delegate and closure")]
	public static ulong LambdaDelegateClosure() {
		ulong result = 0;
		Test testFunc = delegate { return result + 10; };
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + i;
		}

		return result;
	}

	[Benchmark("No Lambda Expression", "Tests a lambda expression using Action")]
	public static ulong LambdaAction() {
		ulong result = 0;
		Action<ulong> test = delegate(ulong param) { result = FuncTestAction(param); };
		for (ulong i = 0; i < LoopIterations; i++) {
			test(result + i);
		}

		return result;
	}

	public static ulong FuncTest() {
		return 10;
	}

	public static ulong FuncTestParam(ulong result) {
		return 10 + result;
	}

	public static ulong FuncTestAction(ulong result) {
		return 10 + result;
	}
}