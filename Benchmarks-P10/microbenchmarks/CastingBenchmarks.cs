﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.benchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class CastingBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Casting", "Tests not casting")]
	public static ulong NoCast() {
		ulong result = 0;
		ulong a = 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = a + i;
		}

		return result;
	}

	[Benchmark("Casting", "Tests implicit casting")]
	public static double Implicit() {
		double result = 0;
		ulong a = 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = a + i;
		}

		return result;
	}

	[Benchmark("Casting", "Tests implicit casting")]
	public static double Explicit() {
		double result = 0;
		ulong a = 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = a + (double)i;
		}

		return result;
	}

	[Benchmark("ReferenceCasting", "Tests explicit casting")]
	public static string ReferenceExplicit() {
		string result = "";
		object o = "Casting";
		for (ulong i = 0; i < LoopIterations; i++) {
			result = (string)o + i;
		}

		return result;
	}

	[Benchmark("ReferenceCasting", "Tests casting using the as keyword")]
	public static string As() {
		string result = "";
		object o = "Casting";
		for (ulong i = 0; i < LoopIterations; i++) {
			result = (o as string) + i;
		}

		return result;
	}
}