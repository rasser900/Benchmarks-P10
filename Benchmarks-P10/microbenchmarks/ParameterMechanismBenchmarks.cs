﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.benchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class ParameterMechanismBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("ParamMech", "Tests using the 'in' parameter modifier")]
	public static ulong In() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + InArg(result) + i;
		}

		return result;
	}

	[Benchmark("ParamMech", "Tests using the 'ref' parameter modifier, without modifying")]
	public static ulong Ref() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + RefArg(ref result) + i;
		}

		return result;
	}

	[Benchmark("ParamMechMod", "Tests using the 'ref' parameter modifier to modify value")]
	public static ulong RefMod() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + RefModArg(ref result, i);
		}

		return result;
	}

	[Benchmark("ParamMechMod", "Tests using the 'out' parameter modifier")]
	public static ulong Out() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + OutArg(out result, i);
		}

		return result;
	}

	[Benchmark("ParamMechReturn", "Tests using the 'ref' parameter modifier to return a value")]
	public static ulong RefReturn() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ReturnRefHelp(ref result, i + result);
			result = result + i;
		}

		return result;
	}

	[Benchmark("ParamMechReturn", "Tests using the 'out' parameter modifier to return a values")]
	public static ulong OutReturn() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ReturnOutHelp(out result, i + result);
			result = result + i;
		}

		return result;
	}

	[Benchmark("ParamMechReturn", "Tests using 'return' to return a value")]
	public static ulong Return() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = ReturnHelp(result, i + result);
			result = result + i;
		}

		return result;
	}

	[Benchmark("ParamMechReturn", "Tests using 'return' to return a value, using alternative approach")]
	public static ulong ReturnAlt() {
		ulong result = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			result = ReturnHelpAlt(i + result);
			result = result + i;
		}

		return result;
	}

	[Benchmark("ParamMechReturnStruct", "Tests using 'return' to return a value")]
	public static ulong ReturnLargeStruct() {
		BigHelperStruct obj = new BigHelperStruct();
		for (ulong i = 0; i < LoopIterations; i++) {
			obj = ReturnModStruct(obj, i);
		}

		return obj.Number;
	}

	[Benchmark("ParamMechReturnStruct", "Tests using 'return' to return a value")]
	public static ulong RefLargeStruct() {
		BigHelperStruct obj = new BigHelperStruct();
		for (ulong i = 0; i < LoopIterations; i++) {
			RefModStruct(ref obj, ref i);
		}

		return obj.Number;
	}

	[Benchmark("ParamMechReturnStruct", "Tests using 'return' to return a value")]
	public static ulong ReturnSmallStruct() {
		SmallHelperStruct obj = new SmallHelperStruct();
		for (ulong i = 0; i < LoopIterations; i++) {
			obj = ReturnModStruct(obj, i);
		}

		return obj.Number;
	}

	[Benchmark("ParamMechReturnStruct", "Tests using 'return' to return a value")]
	public static ulong RefSmallStruct() {
		SmallHelperStruct obj = new SmallHelperStruct();
		for (ulong i = 0; i < LoopIterations; i++) {
			RefModStruct(ref obj, ref i);
		}

		return obj.Number;
	}

	private static ulong InArg(in ulong number) {
		return number;
	}

	private static ulong RefArg(ref ulong number) {
		return number;
	}

	private static ulong RefModArg(ref ulong number, ulong i) {
		number = i;
		return number;
	}

	private static ulong OutArg(out ulong number, ulong i) {
		number = i;
		return number;
	}

	private static void ReturnRefHelp(ref ulong number, ulong index) {
		number = index + index;
	}

	private static void ReturnOutHelp(out ulong number, ulong index) {
		number = index + index;
	}

	private static ulong ReturnHelp(ulong number, ulong index) {
		number = index + index;
		return number;
	}

	private static ulong ReturnHelpAlt(ulong index) {
		return index + index;
	}

	private struct BigHelperStruct {
		public BigHelperStruct() {
			BigArray = new string[100000];
			for (int i = 0; i < 100000; i++) {
				BigArray[i] = i.ToString();
			}

			Number = 0;
		}

		public ulong Number { get; set; }
		public string[] BigArray { get; }
	}

	private struct SmallHelperStruct {
		public SmallHelperStruct() {
			Number = 0;
		}

		public ulong Number { get; set; }
	}

	private static BigHelperStruct ReturnModStruct(BigHelperStruct obj, ulong i) {
		obj.Number += i;
		return obj;
	}

	private static void RefModStruct(ref BigHelperStruct obj, ref ulong i) {
		obj.Number += i;
	}

	private static SmallHelperStruct ReturnModStruct(SmallHelperStruct obj, ulong i) {
		obj.Number += i;
		return obj;
	}

	private static void RefModStruct(ref SmallHelperStruct obj, ref ulong i) {
		obj.Number += i;
	}
}