using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.benchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class LambdaBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Lambda Expression", "Tests a lambda expression using Func<>")]
	public static ulong Lambda() {
		ulong result = 0;
		Func<ulong> testFunc = () => 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = result + testFunc() + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests a lambda expression using Func<> with closure")]
	public static ulong LambdaClosure() {
		ulong result = 0;
		Func<ulong> testFunc = () => result + 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests a lambda expression using Func<> with a param")]
	public static ulong LambdaParam() {
		ulong result = 0;
		Func<ulong, ulong> testFunc = x => 10 + x;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc(result) + i;
		}

		return result;
	}

	private delegate ulong Test();

	[Benchmark("Lambda Expression", "Tests a lambda expression using Delegate")]
	public static ulong LambdaDelegate() {
		ulong result = 0;
		Test testFunc = () => 10;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + result + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests a lambda expression using Delegate and closure")]
	public static ulong LambdaDelegateClosure() {
		ulong result = 0;
		Test testFunc = () => 10 + result;
		for (ulong i = 0; i < LoopIterations; i++) {
			result = testFunc() + i;
		}

		return result;
	}

	[Benchmark("Lambda Expression", "Tests a lambda expression using Action")]
	public static ulong LambdaAction() {
		ulong result = 0;
		Action<ulong> test = param => result = 10 + param;
		for (ulong i = 0; i < LoopIterations; i++) {
			test(result + i);
		}

		return result;
	}
}