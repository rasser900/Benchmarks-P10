using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.microbenchmarks.Concurrency;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class ThreadPoolBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;
	private static ulong?[] _resArr = null!;

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool1() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(1);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1Interlocked() {
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(1);
				Interlocked.Add(ref res, temp);
			});
		}

		while (ThreadPool.PendingWorkItemCount > 0) {
			Thread.Sleep(0);
		}

		return res;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1Countdown() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(1);
				Interlocked.Add(ref res, temp);
				cde.Signal();
			});
		}

		cde.Wait();

		return res;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1Naive() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		var threadPool = new MyThreadPool(Environment.ProcessorCount);
		Action myWork = () => {
			ulong temp = SharedResources.DoSomeWork(1);
			Interlocked.Add(ref res, temp);
			cde.Signal();
		};
		for (ulong i = 0; i < LoopIterations; i++) {
			threadPool.Enqueue(myWork);
		}

		cde.Wait();
		threadPool.Stop();
		return res;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1Specialized() {
		CountdownEvent iterCde = new CountdownEvent((int)LoopIterations);
		CountdownEvent threadCde = new CountdownEvent(Environment.ProcessorCount);
		ulong res = 0;
		MySpecificThreadPool threadPool = new MySpecificThreadPool(Environment.ProcessorCount, subRes => {
			Interlocked.Add(ref res, subRes);
			threadCde.Signal();
		});

		for (ulong i = 0; i < LoopIterations; i++) {
			threadPool.Enqueue(() => {
				ulong temp = SharedResources.DoSomeWork(1);
				iterCde.Signal();
				return temp;
			});
		}

		iterCde.Wait();
		threadPool.Stop();
		threadCde.Wait();

		return res;
	}
	
	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong NoQueue1() {
		var sum = 0ul;

		for (ulong i = 0; i < LoopIterations; i++) {
			sum += SharedResources.DoSomeWork(1);
		}

		return sum;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong NoQueue1WaitCallback() {
		var sum = 0ul;

		WaitCallback waitCallback = _ => sum += SharedResources.DoSomeWork(1);


		for (ulong i = 0; i < LoopIterations; i++) {
			waitCallback(null);
		}

		return sum;
	}
	
	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ConcurrentQueue1() {
		var queue = new ConcurrentQueue<WaitCallback>();
		var sum = 0ul;

		for (ulong i = 0; i < LoopIterations; i++) {
			queue.Enqueue(_ => sum += SharedResources.DoSomeWork(1));
		}

		while (queue.TryDequeue(out var action)) {
			action(null);
		}

		return sum;
	}

	[Benchmark("Concurrency1", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1NoQueue() {
		ulong res = 0;
		MyThreadPoolNoQueue threadPool = new MyThreadPoolNoQueue(Environment.ProcessorCount,
			LoopIterations, _ => {
				var subRes = SharedResources.DoSomeWork(1);
				Interlocked.Add(ref res, subRes);
			});


		threadPool.Wait();

		return res;
	}

	[Benchmark("Concurrency10", "Tests using thread pool")]
	public static ulong ThreadPool10() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(10);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency10", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool10Interlocked() {
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(10);
				Interlocked.Add(ref res, temp);
			});
		}

		while (ThreadPool.PendingWorkItemCount > 0) {
			Thread.Sleep(0);
		}

		return res;
	}

	[Benchmark("Concurrency10", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool10Countdown() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(10);
				Interlocked.Add(ref res, temp);
				cde.Signal();
			});
		}

		cde.Wait();

		return res;
	}

	[Benchmark("Concurrency10", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool10Naive() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		var threadPool = new MyThreadPool(Environment.ProcessorCount);
		Action myWork = () => {
			ulong temp = SharedResources.DoSomeWork(10);
			Interlocked.Add(ref res, temp);
			cde.Signal();
		};
		for (ulong i = 0; i < LoopIterations; i++) {
			threadPool.Enqueue(myWork);
		}

		cde.Wait();
		threadPool.Stop();
		return res;
	}

	[Benchmark("Concurrency100", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool100() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(100);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency100", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool100Interlocked() {
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(100);
				Interlocked.Add(ref res, temp);
			});
		}

		while (ThreadPool.PendingWorkItemCount > 0) {
			Thread.Sleep(0);
		}

		return res;
	}

	[Benchmark("Concurrency100", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool100Countdown() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(100);
				Interlocked.Add(ref res, temp);
				cde.Signal();
			});
		}

		cde.Wait();

		return res;
	}

	[Benchmark("Concurrency100", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool100Naive() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		var threadPool = new MyThreadPool(Environment.ProcessorCount);
		Action myWork = () => {
			ulong temp = SharedResources.DoSomeWork(100);
			Interlocked.Add(ref res, temp);
			cde.Signal();
		};
		for (ulong i = 0; i < LoopIterations; i++) {
			threadPool.Enqueue(myWork);
		}

		cde.Wait();
		threadPool.Stop();
		return res;
	}

	[Benchmark("Concurrency1000", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool1000() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(1000);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency1000", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1000Interlocked() {
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(1000);
				Interlocked.Add(ref res, temp);
			});
		}

		while (ThreadPool.PendingWorkItemCount > 0) {
			Thread.Sleep(0);
		}

		return res;
	}

	[Benchmark("Concurrency1000", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1000Countdown() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(1000);
				Interlocked.Add(ref res, temp);
				cde.Signal();
			});
		}

		cde.Wait();

		return res;
	}

	[Benchmark("Concurrency1000", "Tests using thread pool", plotOrder: 15, skip: true)]
	public static ulong ThreadPool1000Naive() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		var threadPool = new MyThreadPool(Environment.ProcessorCount);
		Action myWork = () => {
			ulong temp = SharedResources.DoSomeWork(1000);
			Interlocked.Add(ref res, temp);
			cde.Signal();
		};
		for (ulong i = 0; i < LoopIterations; i++) {
			threadPool.Enqueue(myWork);
		}

		cde.Wait();
		threadPool.Stop();
		return res;
	}

	[Benchmark("Concurrency10k", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool10k() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(10000);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency100k", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool100k() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(100000);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency1mil", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool1mil() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(1000000);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency10mil", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool10mil() {
		_resArr = new ulong?[LoopIterations];

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(o => {
				ulong index = (ulong)o!;
				_resArr[index] = SharedResources.DoSomeWork(10000000);
			}, i);
		}

		ulong res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			while (!_resArr[i].HasValue) {
				Thread.Sleep(0);
			}

			res += _resArr[i]!.Value;
		}

		return res;
	}

	[Benchmark("Concurrency10mil", "Tests using thread pool", plotOrder: 15)]
	public static ulong ThreadPool10milCountdown() {
		CountdownEvent cde = new CountdownEvent((int)LoopIterations);
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			ThreadPool.QueueUserWorkItem(_ => {
				ulong temp = SharedResources.DoSomeWork(10000000);
				Interlocked.Add(ref res, temp);
				cde.Signal();
			});
		}

		cde.Wait();

		return res;
	}

	private class MyThreadPool {
		private ConcurrentQueue<Action> _queue = new();
		private bool _isRunning = true;

		public MyThreadPool(int numThreads) {
			for (var i = 0; i < numThreads; i++) {
				var thread = new Thread(() => {
					while (_isRunning) {
						if (_queue.TryDequeue(out var action)) {
							action();
						}
						else {
							Thread.Sleep(1);
						}
					}
				});
				thread.Start();
			}
		}

		public void Enqueue(Action action) {
			_queue.Enqueue(action);
		}

		public void Stop() {
			_isRunning = false;
		}
	}

	private class MySpecificThreadPool {
		private ConcurrentQueue<Func<ulong>> _queue = new();
		private bool _isRunning = true;

		public MySpecificThreadPool(int numThreads, Action<ulong> final) {
			for (var i = 0; i < numThreads; i++) {
				var thread = new Thread(() => {
					ulong res = 0;
					while (_isRunning) {
						if (_queue.TryDequeue(out var func)) {
							res += func();
						}
						else {
							Thread.Sleep(1);
						}
					}

					final(res);
				});
				thread.Start();
			}
		}

		public void Enqueue(Func<ulong> func) {
			_queue.Enqueue(func);
		}

		public void Stop() {
			_isRunning = false;
		}
	}

	private class MyThreadPoolNoQueue {
		private readonly ulong _loopIterations;
		private readonly WaitCallback _work;
		private ulong _index = 0;
		private readonly CountdownEvent _cde;

		public MyThreadPoolNoQueue(int numThreads, ulong loopIterations, WaitCallback work) {
			_loopIterations = loopIterations;
			_work = work;
			_cde = new CountdownEvent(numThreads);
			for (var i = 0; i < numThreads; i++) {
				var thread = new Thread(() => {
					while (Interlocked.Increment(ref _index) < _loopIterations) {
						_work(null);
					}

					_cde.Signal();
				});
				thread.Start();
			}
		}

		public void Wait() {
			_cde.Wait();
		}
	}
}