﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.microbenchmarks.Concurrency;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class Concurrency10 {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Concurrency10", "Tests simple Parallel.For", plotOrder: 1)]
	public long ParallelForDefault() {
		long res = 0;

		Parallel.For<long>(0L, (long)LoopIterations, () => 0, (_, _, subtotal) => {
				subtotal += (long)SharedResources.DoSomeWork(10);

				return subtotal;
			}, x => Interlocked.Add(ref res, x)
		);
		return res;
	}

	[Benchmark("Concurrency10", "Tests simple Parallel.For", plotOrder: 2)]
	public long ParallelFor2t() {
		long res = 0;

		Parallel.For<long>(0L, (long)LoopIterations, new ParallelOptions { MaxDegreeOfParallelism = 2 }, () => 0,
			(_, _, subtotal) => {
				subtotal += (long)SharedResources.DoSomeWork(10);

				return subtotal;
			}, x => Interlocked.Add(ref res, x)
		);
		return res;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 3)]
	public ulong ManualThread2() {
		const ulong numThreads = 2;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests simple Parallel.For", plotOrder: 4)]
	public long ParallelFor3t() {
		long res = 0;

		Parallel.For<long>(0L, (long)LoopIterations, new ParallelOptions { MaxDegreeOfParallelism = 3 }, () => 0,
			(_, _, subtotal) => {
				subtotal += (long)SharedResources.DoSomeWork(10);

				return subtotal;
			}, x => Interlocked.Add(ref res, x)
		);
		return res;
	}

	[Benchmark("Concurrency10", "Tests simple Parallel.For", plotOrder: 5)]
	public long ParallelFor4t() {
		long res = 0;

		Parallel.For<long>(0L, (long)LoopIterations, new ParallelOptions { MaxDegreeOfParallelism = 4 }, () => 0,
			(_, _, subtotal) => {
				subtotal += (long)SharedResources.DoSomeWork(10);

				return subtotal;
			}, x => Interlocked.Add(ref res, x)
		);
		return res;
	}


	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 6)]
	public ulong ManualThread4() {
		const ulong numThreads = 4;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests simple Parallel.For", plotOrder: 7)]
	public long ParallelFor6t() {
		long res = 0;

		Parallel.For<long>(0L, (long)LoopIterations, new ParallelOptions { MaxDegreeOfParallelism = 6 }, () => 0,
			(_, _, subtotal) => {
				subtotal += (long)SharedResources.DoSomeWork(10);

				return subtotal;
			}, x => Interlocked.Add(ref res, x)
		);
		return res;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 8)]
	public ulong ManualThread6() {
		const ulong numThreads = 6;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 9)]
	public ulong ManualThread12() {
		const ulong numThreads = 12;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 10)]
	public ulong ManualThread24() {
		const ulong numThreads = 24;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 11)]
	public ulong ManualThread48() {
		const ulong numThreads = 48;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 12)]
	public ulong ManualThread96() {
		const ulong numThreads = 96;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests creating threads manually", plotOrder: 13)]
	public ulong ManualThread192() {
		const ulong numThreads = 192;
		Thread[] tArr = new Thread[numThreads];
		ulong[] resArr = new ulong[numThreads];
		ulong iter = LoopIterations / numThreads;
		ulong firstIter = LoopIterations - (iter * numThreads) + iter;

		for (ulong j = 0; j < numThreads; j++) {
			ulong j1 = j;
			if (j == 0) {
				tArr[j] = new Thread(() => {
					ulong temp = 0;
					for (ulong i = 0; i < firstIter; i++) {
						temp += SharedResources.DoSomeWork(10);
					}

					resArr[j1] = temp;
				});
				tArr[j].Start();
				continue;
			}

			tArr[j] = new Thread(() => {
				ulong temp = 0;
				for (ulong i = 0; i < iter; i++) {
					temp += SharedResources.DoSomeWork(10);
				}

				resArr[j1] = temp;
			});
			tArr[j].Start();
		}

		foreach (Thread thread in tArr) {
			thread.Join();
		}

		ulong sum = 0;
		Array.ForEach(resArr, res => sum += res);

		return sum;
	}

	[Benchmark("Concurrency10", "Tests using tasks", plotOrder: 14)]
	public ulong UsingTasks() {
		ulong res = 0;
		Task<ulong>[] reArr = new Task<ulong>[LoopIterations];
		for (ulong i = 0; i < LoopIterations; i++) {
			reArr[i] = (Task.Run(() => SharedResources.DoSomeWork(10))
				.ContinueWith(task => Interlocked.Add(ref res, task.Result)));
		}

		Task.WaitAll(reArr);

		return res;
	}

	[Benchmark("Concurrency10", "Tests sequential", plotOrder: 16)]
	public ulong Sequential() {
		ulong res = 0;

		for (ulong i = 0; i < LoopIterations; i++) {
			res += SharedResources.DoSomeWork(10);
		}

		return res;
	}
}