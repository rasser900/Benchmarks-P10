#!/bin/bash
if [[ $(id -u) -ne 0 ]]
  then echo "Please run as root"
  exit
fi

#../Scripts/setPerformance.sh
cpupower frequency-set -g performance
cset shield --exec  -- dotnet run --configuration Debug -- -z -p -j
cpupower frequency-set -g powersave
#../Scripts/setPowerSave.sh
