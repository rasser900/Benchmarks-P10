﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class DijkstraBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Dijkstra", "Benchmarking the original Dijkstra Benchmark")]
	public int DijkstraOriginal() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraOriginal.DijkstraOriginal.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark without Linq")]
	public int DijkstraNoLinq() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraNoLinq.DijkstraNoLinq.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark with array")]
	public int DijkstraArray() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraArray.DijkstraArray.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark with struct")]
	public int DijkstraObjects() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraObjects.DijkstraObjects.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark without Linq with array")]
	public int DijkstraNoLinqArray() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraNoLinqArray.DijkstraNoLinqArray.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark without Linq with struct")]
	public int DijkstraNoLinqObjects() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraNoLinqObjects.DijkstraNoLinqObjects.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark with array and struct")]
	public int DijkstraArrayObjects() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraArrayObjects.DijkstraArrayObjects.Run());
		}

		return res;
	}
	
	[Benchmark("Dijkstra", "Benchmarking the Dijkstra Benchmark without Linq with array and struct")]
	public int DijkstraNoLinqArrayObjects() {
		var res = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			res +=(Dijkstra.DijkstraNoLinqArrayObjects.DijkstraNoLinqArrayObjects.Run());
		}

		return res;
	}
	
}