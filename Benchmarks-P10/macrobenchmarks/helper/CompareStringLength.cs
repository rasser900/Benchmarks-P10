﻿using System.Text;

namespace Benchmarks_P10.macrobenchmarks;

public class CompareStringLengthOriginal {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];
			for (int i = 0; i < strings.Length; i++)
				li[i] = (strings[i].Length, i);
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			for (int i = 0; i < strings.Length; i++) {
				int length = li[i].Item1;
				string str = strings[li[i].Item2];
				if (length == maxLength)
					predicate = predicateMax;
				else if (length == minLength)
					predicate = predicateMin;
				else
					predicate = predicateAve;
				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthParallelFor1 {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			Parallel.For(0, strings.Length, i => { li[i] = (strings[i].Length, i); });
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			for (int i = 0; i < strings.Length; i++) {
				int length = li[i].Item1;
				string str = strings[li[i].Item2];
				if (length == maxLength)
					predicate = predicateMax;
				else if (length == minLength)
					predicate = predicateMin;
				else
					predicate = predicateAve;
				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthParallelFor2 {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			for (int i = 0; i < strings.Length; i++)
				li[i] = (strings[i].Length, i);
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			Parallel.For(0, strings.Length, i => {
				if (strings[i].Length == maxLength) {
					predicate = predicateMax;
				}
				else if (strings[i].Length == minLength) {
					predicate = predicateMin;
				}
				else {
					predicate = predicateAve;
				}

				lock (sb) {
					sb.Append(Q + strings[i] + Q + hasLength + strings[i].Length + predicate + "\n");
				}
			});
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelection {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];
			for (int i = 0; i < strings.Length; i++)
				li[i] = (strings[i].Length, i);
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			for (int i = 0; i < strings.Length; i++) {
				int length = li[i].Item1;
				string str = strings[li[i].Item2];
				switch (length) {
					case var _ when length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthForeach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];
			foreach (var element in strings) {
				li[Array.IndexOf(strings, element)] = (element.Length, Array.IndexOf(strings, element));
			}

			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			foreach (var element in strings) {
				int length = li[Array.IndexOf(strings, element)].Item1;
				string str = strings[li[Array.IndexOf(strings, element)].Item2];
				if (length == maxLength)
					predicate = predicateMax;
				else if (length == minLength)
					predicate = predicateMin;
				else
					predicate = predicateAve;
				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelectionForeach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];
			foreach (var element in strings) {
				li[Array.IndexOf(strings, element)] = (element.Length, Array.IndexOf(strings, element));
			}

			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			foreach (var element in strings) {
				int length = li[Array.IndexOf(strings, element)].Item1;
				string str = strings[li[Array.IndexOf(strings, element)].Item2];
				switch (length) {
					case var _ when length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelectionParallelFor1 {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			Parallel.For(0, strings.Length, i => { li[i] = (strings[i].Length, i); });
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			for (int i = 0; i < strings.Length; i++) {
				int length = li[i].Item1;
				string str = strings[li[i].Item2];
				switch (length) {
					case var _ when length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelectionParallelFor2 {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			for (int i = 0; i < strings.Length; i++)
				li[i] = (strings[i].Length, i);
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			Parallel.For(0, strings.Length, i => {
				switch (strings[i].Length) {
					case var _ when strings[i].Length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when strings[i].Length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				lock (sb) {
					sb.Append(Q + strings[i] + Q + hasLength + strings[i].Length + predicate + "\n");
				}
			});
		}

		return sb.ToString();
	}
}

public class CompareStringLengthParallelFor1Foreach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			Parallel.For(0, strings.Length, i => { li[i] = (strings[i].Length, i); });
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			foreach (var element in strings) {
				int length = li[Array.IndexOf(strings, element)].Item1;
				string str = strings[li[Array.IndexOf(strings, element)].Item2];
				if (length == maxLength)
					predicate = predicateMax;
				else if (length == minLength)
					predicate = predicateMin;
				else
					predicate = predicateAve;
				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthParallelFor2Foreach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			foreach (var element in strings) {
				li[Array.IndexOf(strings, element)] = (element.Length, Array.IndexOf(strings, element));
			}

			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			Parallel.For(0, strings.Length, i => {
				if (strings[i].Length == maxLength) {
					predicate = predicateMax;
				}
				else if (strings[i].Length == minLength) {
					predicate = predicateMin;
				}
				else {
					predicate = predicateAve;
				}

				lock (sb) {
					sb.Append(Q + strings[i] + Q + hasLength + strings[i].Length + predicate + "\n");
				}
			});
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelectionParallelFor1Foreach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			Parallel.For(0, strings.Length, i => { li[i] = (strings[i].Length, i); });
			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			foreach (var element in strings) {
				int length = li[Array.IndexOf(strings, element)].Item1;
				string str = strings[li[Array.IndexOf(strings, element)].Item2];
				switch (length) {
					case var _ when length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				sb.Append(Q + str + Q + hasLength + length + predicate + "\n");
			}
		}

		return sb.ToString();
	}
}

public class CompareStringLengthSelectionParallelFor2Foreach {
	public ulong Run() {
		var strings = new string[] { "abcd", "123456789", "abcdef", "1234567" };
		return (ulong)CompareAndReportStringsLength(strings).Length;
	}

	static string CompareAndReportStringsLength(string[] strings) {
		StringBuilder sb = new StringBuilder();
		if (strings.Length > 0) {
			char Q = '"';
			string hasLength = " has length ";
			string predicateMax = " and is the longest string";
			string predicateMin = " and is the shortest string";
			string predicateAve = " and is neither the longest nor the shortest string";
			string predicate;

			(int, int)[] li = new (int, int)[strings.Length];

			foreach (var element in strings) {
				li[Array.IndexOf(strings, element)] = (element.Length, Array.IndexOf(strings, element));
			}

			Array.Sort(li, ((int, int) a, (int, int) b) => b.Item1 - a.Item1);
			int maxLength = li[0].Item1;
			int minLength = li[strings.Length - 1].Item1;

			Parallel.For(0, strings.Length, i => {
				switch (strings[i].Length) {
					case var _ when strings[i].Length == maxLength:
						predicate = predicateMax;
						break;
					case var _ when strings[i].Length == minLength:
						predicate = predicateMin;
						break;
					default:
						predicate = predicateAve;
						break;
				}

				lock (sb) {
					sb.Append(Q + strings[i] + Q + hasLength + strings[i].Length + predicate + "\n");
				}
			});
		}

		return sb.ToString();
	}
}