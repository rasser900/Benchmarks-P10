﻿using System.Text;
using static System.Linq.Enumerable;
using static System.String;
using static System.Console;
using EdgeList = System.Collections.Generic.List<(int node, double weight)>;

namespace Benchmarks_P10.macrobenchmarks.Dijkstra {
	namespace DijkstraOriginal {
		public class DijkstraOriginal {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					sb.Append(Join(" -> ", Path(id('a'), d).Select(p => $"{name(p.node)}({p.distance})").Reverse()));
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		sealed class Graph {
			private readonly List<EdgeList> adjacency;

			public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();

			public int Count => adjacency.Count;
			public bool HasEdge(int s, int e) => adjacency[s].Any(p => p.node == e);
			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				var info = Range(0, adjacency.Count).Select(i => (distance: double.PositiveInfinity, prev: i))
					.ToArray();

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Count);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraNoLinq {
		public class DijkstraNoLinq {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					var localPath = Path(id('a'), d);
					var stack = new Stack<string>();
					foreach ((double distance, int node) in localPath) {
						stack.Push($"{name(node)}({distance})");
					}

					while (stack.Count > 1) {
						sb.Append(stack.Pop());
						sb.Append(" -> ");
					}

					sb.Append(stack.Pop());
					sb.AppendLine();
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		sealed class Graph {
			private readonly List<EdgeList> adjacency;

			//public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();
			public Graph(int vertexCount) {
				adjacency = new();
				for (int i = 0; i < vertexCount; i++) {
					adjacency.Add(new EdgeList());
				}
			}

			public int Count => adjacency.Count;

			public bool HasEdge(int s, int e) {
				return adjacency[s].Contains((s, e));
			}

			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				(double distance, int prev)[] info = new (double distance, int prev)[adjacency.Count];

				for (int i = 0; i < adjacency.Count; i++) {
					info[i] = (distance: double.PositiveInfinity, prev: i);
				}

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Count);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraArray {
		public class DijkstraArray {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					sb.Append(Join(" -> ", Path(id('a'), d).Select(p => $"{name(p.node)}({p.distance})").Reverse()));
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		sealed class Graph {
			private readonly EdgeList[] adjacency;

			public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToArray();

			public int Count => adjacency.Length;
			public bool HasEdge(int s, int e) => adjacency[s].Any(p => p.node == e);
			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				var info = Range(0, adjacency.Length).Select(i => (distance: double.PositiveInfinity, prev: i))
					.ToArray();

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Length);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraObjects {
		public class DijkstraObjects {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					sb.Append(Join(" -> ", Path(id('a'), d).Select(p => $"{name(p.node)}({p.distance})").Reverse()));
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		internal struct Graph {
			private readonly List<EdgeList> adjacency;

			public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();

			public int Count => adjacency.Count;
			public bool HasEdge(int s, int e) => adjacency[s].Any(p => p.node == e);
			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				var info = Range(0, adjacency.Count).Select(i => (distance: double.PositiveInfinity, prev: i))
					.ToArray();

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Count);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraNoLinqArray {
		public class DijkstraNoLinqArray {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					var localPath = Path(id('a'), d);
					var stack = new Stack<string>();
					foreach ((double distance, int node) in localPath) {
						stack.Push($"{name(node)}({distance})");
					}

					while (stack.Count > 1) {
						sb.Append(stack.Pop());
						sb.Append(" -> ");
					}

					sb.Append(stack.Pop());
					sb.AppendLine();
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		sealed class Graph {
			private readonly EdgeList[] adjacency;

			//public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();
			public Graph(int vertexCount) {
				adjacency = new EdgeList[vertexCount];
				for (int i = 0; i < vertexCount; i++) {
					adjacency[i] = new EdgeList();
				}
			}

			public int Count => adjacency.Length;

			public bool HasEdge(int s, int e) {
				return adjacency[s].Contains((s, e));
			}

			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				(double distance, int prev)[] info = new (double distance, int prev)[adjacency.Length];

				for (int i = 0; i < adjacency.Length; i++) {
					info[i] = (distance: double.PositiveInfinity, prev: i);
				}

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Length);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraNoLinqObjects {
		public class DijkstraNoLinqObjects {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					var localPath = Path(id('a'), d);
					var stack = new Stack<string>();
					foreach ((double distance, int node) in localPath) {
						stack.Push($"{name(node)}({distance})");
					}

					while (stack.Count > 1) {
						sb.Append(stack.Pop());
						sb.Append(" -> ");
					}

					sb.Append(stack.Pop());
					sb.AppendLine();
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		internal struct Graph {
			private readonly List<EdgeList> adjacency;

			//public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();
			public Graph(int vertexCount) {
				adjacency = new();
				for (int i = 0; i < vertexCount; i++) {
					adjacency.Add(new EdgeList());
				}
			}

			public int Count => adjacency.Count;

			public bool HasEdge(int s, int e) {
				return adjacency[s].Contains((s, e));
			}

			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				(double distance, int prev)[] info = new (double distance, int prev)[adjacency.Count];

				for (int i = 0; i < adjacency.Count; i++) {
					info[i] = (distance: double.PositiveInfinity, prev: i);
				}

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Count);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraArrayObjects {
		public class DijkstraArrayObjects {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					sb.Append(Join(" -> ", Path(id('a'), d).Select(p => $"{name(p.node)}({p.distance})").Reverse()));
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		internal struct Graph {
			private readonly EdgeList[] adjacency;

			public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToArray();

			public int Count => adjacency.Length;
			public bool HasEdge(int s, int e) => adjacency[s].Any(p => p.node == e);
			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				var info = Range(0, adjacency.Length).Select(i => (distance: double.PositiveInfinity, prev: i))
					.ToArray();

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Length);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}

	namespace DijkstraNoLinqArrayObjects {
		public class DijkstraNoLinqArrayObjects {
			public static int Run() {
				var sb = new StringBuilder();
				Graph graph = new Graph(6);
				Func<char, int> id = c => c - 'a';
				Func<int, char> name = i => (char)(i + 'a');
				foreach (var (start, end, cost) in new[] {
						('a', 'b', 7),
						('a', 'c', 9),
						('a', 'f', 14),
						('b', 'c', 10),
						('b', 'd', 15),
						('c', 'd', 11),
						('c', 'f', 2),
						('d', 'e', 6),
						('e', 'f', 9),
					}) {
					graph.AddEdge(id(start), id(end), cost);
				}

				var path = graph.FindPath(id('a'));
				for (int d = id('b'); d <= id('f'); d++) {
					var localPath = Path(id('a'), d);
					var stack = new Stack<string>();
					foreach ((double distance, int node) in localPath) {
						stack.Push($"{name(node)}({distance})");
					}

					while (stack.Count > 1) {
						sb.Append(stack.Pop());
						sb.Append(" -> ");
					}

					sb.Append(stack.Pop());
					sb.AppendLine();
				}

				IEnumerable<(double distance, int node)> Path(int start, int destination) {
					yield return (path[destination].distance, destination);
					for (int i = destination; i != start; i = path[i].prev) {
						yield return (path[path[i].prev].distance, path[i].prev);
					}
				}

				return sb.Length;
			}
		}

		internal struct Graph {
			private readonly EdgeList[] adjacency;

			//public Graph(int vertexCount) => adjacency = Range(0, vertexCount).Select(v => new EdgeList()).ToList();
			public Graph(int vertexCount) {
				adjacency = new EdgeList[vertexCount];
				for (int i = 0; i < vertexCount; i++) {
					adjacency[i] = new EdgeList();
				}
			}

			public int Count => adjacency.Length;

			public bool HasEdge(int s, int e) {
				return adjacency[s].Contains((s, e));
			}

			public bool RemoveEdge(int s, int e) => adjacency[s].RemoveAll(p => p.node == e) > 0;

			public bool AddEdge(int s, int e, double weight) {
				if (HasEdge(s, e)) return false;
				adjacency[s].Add((e, weight));
				return true;
			}

			public (double distance, int prev)[] FindPath(int start) {
				(double distance, int prev)[] info = new (double distance, int prev)[adjacency.Length];

				for (int i = 0; i < adjacency.Length; i++) {
					info[i] = (distance: double.PositiveInfinity, prev: i);
				}

				info[start].distance = 0;
				var visited = new System.Collections.BitArray(adjacency.Length);

				var heap = new Heap<(int node, double distance)>((a, b) => a.distance.CompareTo(b.distance));
				heap.Push((start, 0));
				while (heap.Count > 0) {
					var current = heap.Pop();
					if (visited[current.node]) continue;
					var edges = adjacency[current.node];
					for (int n = 0; n < edges.Count; n++) {
						int v = edges[n].node;
						if (visited[v]) continue;
						double alt = info[current.node].distance + edges[n].weight;
						if (alt < info[v].distance) {
							info[v] = (alt, current.node);
							heap.Push((v, alt));
						}
					}

					visited[current.node] = true;
				}

				return info;
			}
		}

		sealed class Heap<T> {
			private readonly IComparer<T> comparer;
			private readonly List<T> list = new List<T> { default };

			public Heap() : this(default(IComparer<T>)) { }

			public Heap(IComparer<T> comparer) {
				this.comparer = comparer ?? Comparer<T>.Default;
			}

			public Heap(Comparison<T> comparison) : this(Comparer<T>.Create(comparison)) { }

			public int Count => list.Count - 1;

			public void Push(T element) {
				list.Add(element);
				SiftUp(list.Count - 1);
			}

			public T Pop() {
				T result = list[1];
				list[1] = list[list.Count - 1];
				list.RemoveAt(list.Count - 1);
				SiftDown(1);
				return result;
			}

			private static int Parent(int i) => i / 2;
			private static int Left(int i) => i * 2;
			private static int Right(int i) => i * 2 + 1;

			private void SiftUp(int i) {
				while (i > 1) {
					int parent = Parent(i);
					if (comparer.Compare(list[i], list[parent]) > 0) return;
					(list[parent], list[i]) = (list[i], list[parent]);
					i = parent;
				}
			}

			private void SiftDown(int i) {
				for (int left = Left(i); left < list.Count; left = Left(i)) {
					int smallest = comparer.Compare(list[left], list[i]) <= 0 ? left : i;
					int right = Right(i);
					if (right < list.Count && comparer.Compare(list[right], list[smallest]) <= 0) smallest = right;
					if (smallest == i) return;
					(list[i], list[smallest]) = (list[smallest], list[i]);
					i = smallest;
				}
			}
		}
	}
}