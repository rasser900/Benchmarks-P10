﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Benchmarks_P10.macrobenchmarks;

public class AlmostPrimeOriginal {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			kprime.GetFirstN(10).ForEach(num => partialRes += num);
			res += partialRes;
		}

		return res;
	}

	class KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public List<int> GetFirstN(int n) {
			List<int> result = new List<int>();
			for (int number = 2; result.Count < n; ++number) {
				if (IsKPrime(number)) {
					result.Add(number);
				}
			}

			return result;
		}
	}
}

public class AlmostPrimeManualThread {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			kprime.GetFirstN(10).ForEach(num => partialRes += num);
			res += partialRes;
		}

		return res;
	}

	class KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public List<int> GetFirstN(int n) {
			ConcurrentBag<int> result = new ConcurrentBag<int>();


			var pCount = Environment.ProcessorCount;
			var threads = new Thread[pCount];
			int num = 1;
			for (int i = 0; i < pCount; i++) {
				threads[i] = new Thread(_ => {
					while (result.Count < n) {
						var val = Interlocked.Increment(ref num);
						if (IsKPrime(val)) {
							result.Add(val);
						}
					}
				});
				threads[i].Start();
			}

			foreach (var thread in threads) {
				thread.Join();
			}

			var res = new List<int>(result);
			res.Sort();
			var diff = res.Count - n;
			if (diff > 0) {
				res.RemoveRange(n, diff);
			}

			return res;
		}
	}
}

public class AlmostPrimeArray {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			foreach (var elem in kprime.GetFirstN(10)) {
				partialRes += elem;
			}

			res += partialRes;
		}

		return res;
	}

	class KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public int[] GetFirstN(int n) {
			//Get first N primes using Array.
			int[] result = new int[n];
			int primesCount = 0;
			for (int i = 2; primesCount < n; ++i) {
				if (IsKPrime(i)) {
					result[primesCount++] = i;
				}
			}

			return result;
		}
	}
}

public class AlmostPrimeStruct {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			kprime.GetFirstN(10).ForEach(num => partialRes += num);
			res += partialRes;
		}

		return res;
	}

	struct KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public List<int> GetFirstN(int n) {
			List<int> result = new List<int>();
			for (int number = 2; result.Count < n; ++number) {
				if (IsKPrime(number)) {
					result.Add(number);
				}
			}

			return result;
		}
	}
}

public class AlmostPrimeManualThreadArray {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			foreach (var elem in kprime.GetFirstN(10)) {
				partialRes += elem;
			}
			res += partialRes;
		}

		return res;
	}

	class KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public int[] GetFirstN(int n) {
			ConcurrentBag<int> result = new ConcurrentBag<int>();


			var pCount = Environment.ProcessorCount;
			var threads = new Thread[pCount];
			int num = 1;
			for (int i = 0; i < pCount; i++) {
				threads[i] = new Thread(_ => {
					while (result.Count < n) {
						var val = Interlocked.Increment(ref num);
						if (IsKPrime(val)) {
							result.Add(val);
						}
					}
				});
				threads[i].Start();
			}

			foreach (var thread in threads) {
				thread.Join();
			}

			var res = result.ToArray();
			Array.Sort(res);

			var diff = res.Length - n;
			if (diff > 0) {
				//removes elements from n to n + diff, without using Linq
				for (int i = 0; i < diff; i++) {
					res[n + i] = 0;
				}
			}

			return res;
		}
	}
}

public class AlmostPrimeManualThreadStruct {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			kprime.GetFirstN(10).ForEach(num => partialRes += num);
			res += partialRes;
		}

		return res;
	}

	struct KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public List<int> GetFirstN(int n) {
			ConcurrentBag<int> result = new ConcurrentBag<int>();


			var pCount = Environment.ProcessorCount;
			var threads = new Thread[pCount];
			int num = 1;
			for (int i = 0; i < pCount; i++) {
				KPrime tmpThis = this;
				threads[i] = new Thread(_ => {
					while (result.Count < n) {
						var val = Interlocked.Increment(ref num);
						if (tmpThis.IsKPrime(val)) {
							result.Add(val);
						}
					}
				});
				threads[i].Start();
			}

			foreach (var thread in threads) {
				thread.Join();
			}

			var res = new List<int>(result);
			res.Sort();
			var diff = res.Count - n;
			if (diff > 0) {
				res.RemoveRange(n, diff);
			}

			return res;
		}
	}
}

public class AlmostPrimeArrayStruct {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			foreach (var elem in kprime.GetFirstN(10)) {
				partialRes += elem;
			}

			res += partialRes;
		}

		return res;
	}

	struct KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public int[] GetFirstN(int n) {
			//Get first N primes using Array.
			int[] result = new int[n];
			int primesCount = 0;
			for (int i = 2; primesCount < n; ++i) {
				if (IsKPrime(i)) {
					result[primesCount++] = i;
				}
			}

			return result;
		}
	}
}

public class AlmostPrimeManualThreadArrayStruct {
	public long Run() {
		long res = 0;
		foreach (int k in Enumerable.Range(1, 10)) {
			KPrime kprime = new KPrime { K = k };
			long partialRes = 0;
			foreach (var elem in kprime.GetFirstN(10)) {
				partialRes += elem;
			}

			res += partialRes;
		}

		return res;
	}

	class KPrime {
		public int K { get; set; }

		public bool IsKPrime(int number) {
			int primes = 0;
			for (int p = 2; p * p <= number && primes < K; ++p) {
				while (number % p == 0 && primes < K) {
					number /= p;
					++primes;
				}
			}

			if (number > 1) {
				++primes;
			}

			return primes == K;
		}

		public int[] GetFirstN(int n) {
			ConcurrentBag<int> result = new ConcurrentBag<int>();


			var pCount = Environment.ProcessorCount;
			var threads = new Thread[pCount];
			int num = 1;
			for (int i = 0; i < pCount; i++) {
				threads[i] = new Thread(_ => {
					while (result.Count < n) {
						var val = Interlocked.Increment(ref num);
						if (IsKPrime(val)) {
							result.Add(val);
						}
					}
				});
				threads[i].Start();
			}

			foreach (var thread in threads) {
				thread.Join();
			}

			var res = result.ToArray();
			Array.Sort(res);

			var diff = res.Length - n;
			if (diff > 0) {
				//removes elements from n to n + diff, without using Linq
				for (int i = 0; i < diff; i++) {
					res[n + i] = 0;
				}
			}

			return res;
		}
	}
}