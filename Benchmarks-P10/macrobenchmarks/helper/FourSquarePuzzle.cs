﻿namespace Benchmarks_P10.macrobenchmarks;

public class FourSquarePuzzleOriginal {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (int a = low; a <= high; ++a) {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;

				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						if (notValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								if (notValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;

								for (int g = low; g <= high; ++g) {
									if (notValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;

									++count;
									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		return unique && haystack.Any(p => p == needle);
	}
}

public class FourSquarePuzzleParallelFor {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		Parallel.For(low, high + 1, a => {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;
				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						if (notValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								if (notValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;
								for (int g = low; g <= high; ++g) {
									if (notValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;
									Interlocked.Increment(ref count);

									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		});

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		return unique && haystack.Any(p => p == needle);
	}
}

public class FourSquarePuzzleNoLINQ {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (int a = low; a <= high; ++a) {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;

				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						if (notValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								if (notValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;

								for (int g = low; g <= high; ++g) {
									if (notValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;

									++count;
									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		if (unique) {
			foreach (int i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}

public class FourSquarePuzzleSelection {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (int a = low; a <= high; ++a) {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;

				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						switch (unique) {
							case var _ when notValid(unique, d, c, b, a):
								continue;
							case var _ when (fp != b + c + d):
								continue;
						}

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								switch (unique) {
									case var _ when notValid(unique, f, e, d, c, b, a):
										continue;
									case var _ when (fp != d + e + f):
										continue;
								}

								for (int g = low; g <= high; ++g) {
									switch (unique) {
										case var _ when notValid(unique, g, f, e, d, c, b, a):
											continue;
										case var _ when (fp != f + g):
											continue;
									}

									++count;
									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		return unique && haystack.Any(p => p == needle);
	}
}

public class FourSquarePuzzleParallelForNoLINQ {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		Parallel.For(low, high + 1, a => {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;
				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						if (notValid(unique, d, c, b, a)) continue;
						if (fp != b + c + d) continue;

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								if (notValid(unique, f, e, d, c, b, a)) continue;
								if (fp != d + e + f) continue;
								for (int g = low; g <= high; ++g) {
									if (notValid(unique, g, f, e, d, c, b, a)) continue;
									if (fp != f + g) continue;
									Interlocked.Increment(ref count);

									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		});

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		if (unique) {
			foreach (int i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}

public class FourSquarePuzzleParallelForSelection {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		Parallel.For(low, high + 1, a => {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;
				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						switch (unique) {
							case var _ when notValid(unique, d, c, b, a):
								continue;
							case var _ when (fp != b + c + d):
								continue;
						}

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								switch (unique) {
									case var _ when notValid(unique, f, e, d, c, b, a):
										continue;
									case var _ when (fp != d + e + f):
										continue;
								}

								for (int g = low; g <= high; ++g) {
									switch (unique) {
										case var _ when notValid(unique, g, f, e, d, c, b, a):
											continue;
										case var _ when (fp != f + g):
											continue;
									}

									Interlocked.Increment(ref count);

									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		});

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		return unique && haystack.Any(p => p == needle);
	}
}

public class FourSquarePuzzleNoLINQSelection {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		for (int a = low; a <= high; ++a) {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;

				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						switch (unique) {
							case var _ when notValid(unique, d, c, b, a):
								continue;
							case var _ when (fp != b + c + d):
								continue;
						}

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								switch (unique) {
									case var _ when notValid(unique, f, e, d, c, b, a):
										continue;
									case var _ when (fp != d + e + f):
										continue;
								}

								for (int g = low; g <= high; ++g) {
									switch (unique) {
										case var _ when notValid(unique, g, f, e, d, c, b, a):
											continue;
										case var _ when (fp != f + g):
											continue;
									}

									++count;
									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		}

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		if (unique) {
			foreach (int i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}

public class FourSquarePuzzleParallelForNoLINQSelection {
	public ulong Run() {
		var result = 0;
		result += fourSquare(1, 7, true, false);
		result += fourSquare(3, 9, true, false);
		result += fourSquare(0, 9, false, false);
		return (ulong)result;
	}

	private static int fourSquare(int low, int high, bool unique, bool print) {
		int count = 0;

		if (print) {
			Console.WriteLine("a b c d e f g");
		}

		Parallel.For(low, high + 1, a => {
			for (int b = low; b <= high; ++b) {
				if (notValid(unique, b, a)) continue;
				int fp = a + b;
				for (int c = low; c <= high; ++c) {
					if (notValid(unique, c, b, a)) continue;
					for (int d = low; d <= high; ++d) {
						switch (unique) {
							case var _ when notValid(unique, d, c, b, a):
								continue;
							case var _ when (fp != b + c + d):
								continue;
						}

						for (int e = low; e <= high; ++e) {
							if (notValid(unique, e, d, c, b, a)) continue;
							for (int f = low; f <= high; ++f) {
								switch (unique) {
									case var _ when notValid(unique, f, e, d, c, b, a):
										continue;
									case var _ when (fp != d + e + f):
										continue;
								}

								for (int g = low; g <= high; ++g) {
									switch (unique) {
										case var _ when notValid(unique, g, f, e, d, c, b, a):
											continue;
										case var _ when (fp != f + g):
											continue;
									}

									Interlocked.Increment(ref count);

									if (print) {
										Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}", a, b, c, d, e, f, g);
									}
								}
							}
						}
					}
				}
			}
		});

		if (unique) {
			//Console.WriteLine("There are {0} unique solutions in [{1}, {2}]", count, low, high);
		}
		else {
			//Console.WriteLine("There are {0} non-unique solutions in [{1}, {2}]", count, low, high);
		}

		return count;
	}

	private static bool notValid(bool unique, int needle, params int[] haystack) {
		if (unique) {
			foreach (int i in haystack) {
				if (i == needle) {
					return true;
				}
			}
		}

		return false;
	}
}