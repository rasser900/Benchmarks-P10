﻿namespace Benchmarks_P10.macrobenchmarks;

public class Prisoners {
	private static int _seed = 1;

	static bool PlayOptimal() {
		var rnd = new Random(_seed);
		var secrets = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToList();

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom() {
		var rnd = new Random(_seed);
		for (int p = 0; p < 100; p++) {
			var choices = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToList();

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<bool> play) {
		uint success = 0;
		for (int i = 0; i < n; i++) {
			_seed = i;
			if (play()) {
				success++;
			}
		}

		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersParallelFor {
	static bool PlayOptimal(int seed) {
		var rnd = new Random(seed);
		var secrets = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToList();

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom(int seed) {
		var rnd = new Random(seed);

		for (int p = 0; p < 100; p++) {
			var choices = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToList();

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<int, bool> play) {
		uint success = 0;

		Parallel.For(0, (int)n, index => {
			if (play(index)) {
				Interlocked.Increment(ref success);
			}
		});
		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersNoLinq {
	private static int _seed = 1;

	static bool PlayOptimal() {
		var rnd = new Random(_seed);
		var keys = new int[100];
		for (int i = 0; i < 100; i++) {
			keys[i] = rnd.Next();
		}

		var secretsArr = new int[100];
		for (int i = 0; i < 100; i++) {
			secretsArr[i] = i;
		}

		Array.Sort(keys, secretsArr);
		var secrets = new List<int>(secretsArr);

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom() {
		var rnd = new Random(_seed);

		for (int p = 0; p < 100; p++) {
			var keys = new int[100];
			for (int i = 0; i < 100; i++) {
				keys[i] = rnd.Next();
			}

			var secretsArr = new int[100];
			for (int i = 0; i < 100; i++) {
				secretsArr[i] = i;
			}

			Array.Sort(keys, secretsArr);
			var choices = new List<int>(secretsArr);
			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<bool> play) {
		uint success = 0;
		for (int i = 0; i < n; i++) {
			_seed = i;
			if (play()) {
				success++;
			}
		}

		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersArray {
	private static int _seed = 1;

	static bool PlayOptimal() {
		var rnd = new Random(_seed);
		var secrets = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToArray();

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom() {
		var rnd = new Random(_seed);
		for (int p = 0; p < 100; p++) {
			var choices = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToArray();

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<bool> play) {
		uint success = 0;
		for (int i = 0; i < n; i++) {
			_seed = i;
			if (play()) {
				success++;
			}
		}

		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersParallelForNoLinq {
	static bool PlayOptimal(int seed) {
		var rnd = new Random(seed);

		var keys = new int[100];
		for (int i = 0; i < 100; i++) {
			keys[i] = rnd.Next();
		}

		var secretsArr = new int[100];
		for (int i = 0; i < 100; i++) {
			secretsArr[i] = i;
		}

		Array.Sort(keys, secretsArr);
		var secrets = new List<int>(secretsArr);

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom(int seed) {
		var rnd = new Random(seed);

		for (int p = 0; p < 100; p++) {
			var keys = new int[100];

			for (int i = 0; i < 100; i++) {
				keys[i] = rnd.Next();
			}

			var choicesArr = new int[100];
			for (int i = 0; i < 100; i++) {
				choicesArr[i] = i;
			}

			Array.Sort(keys, choicesArr);
			var choices = new List<int>(choicesArr);

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<int, bool> play) {
		uint success = 0;

		Parallel.For(0, (int)n, index => {
			if (play(index)) {
				Interlocked.Increment(ref success);
			}
		});
		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersParallelForArray {
	static bool PlayOptimal(int seed) {
		var rnd = new Random(seed);
		var secrets = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToArray();

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom(int seed) {
		var rnd = new Random(seed);

		for (int p = 0; p < 100; p++) {
			var choices = Enumerable.Range(0, 100).OrderBy(a => rnd.Next()).ToArray();

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<int, bool> play) {
		uint success = 0;

		Parallel.For(0, (int)n, index => {
			if (play(index)) {
				Interlocked.Increment(ref success);
			}
		});
		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersArrayNoLinq {
	private static int _seed = 1;

	static bool PlayOptimal() {
		var rnd = new Random(_seed);
		var keys = new int[100];
		for (int i = 0; i < 100; i++) {
			keys[i] = rnd.Next();
		}

		var secrets = new int[100];
		for (int i = 0; i < 100; i++) {
			secrets[i] = i;
		}

		Array.Sort(keys, secrets);

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom() {
		var rnd = new Random(_seed);

		for (int p = 0; p < 100; p++) {
			var keys = new int[100];
			for (int i = 0; i < 100; i++) {
				keys[i] = rnd.Next();
			}

			var choices = new int[100];
			for (int i = 0; i < 100; i++) {
				choices[i] = i;
			}

			Array.Sort(keys, choices);
			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<bool> play) {
		uint success = 0;
		for (int i = 0; i < n; i++) {
			_seed = i;
			if (play()) {
				success++;
			}
		}

		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}

public class PrisonersParallelForArrayNoLinq {
	static bool PlayOptimal(int seed) {
		var rnd = new Random(seed);

		var keys = new int[100];
		for (int i = 0; i < 100; i++) {
			keys[i] = rnd.Next();
		}

		var secrets = new int[100];
		for (int i = 0; i < 100; i++) {
			secrets[i] = i;
		}

		Array.Sort(keys, secrets);

		for (int p = 0; p < 100; p++) {
			bool success = false;

			var choice = p;
			for (int i = 0; i < 50; i++) {
				if (secrets[choice] == p) {
					success = true;
					break;
				}

				choice = secrets[choice];
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static bool PlayRandom(int seed) {
		var rnd = new Random(seed);

		for (int p = 0; p < 100; p++) {
			var keys = new int[100];

			for (int i = 0; i < 100; i++) {
				keys[i] = rnd.Next();
			}

			var choices = new int[100];
			for (int i = 0; i < 100; i++) {
				choices[i] = i;
			}

			Array.Sort(keys, choices);

			bool success = false;
			for (int i = 0; i < 50; i++) {
				if (choices[i] == p) {
					success = true;
					break;
				}
			}

			if (!success) {
				return false;
			}
		}

		return true;
	}

	static double Exec(uint n, Func<int, bool> play) {
		uint success = 0;

		Parallel.For(0, (int)n, index => {
			if (play(index)) {
				Interlocked.Increment(ref success);
			}
		});
		return 100.0 * success / n;
	}

	public (uint, double, double) Run() {
		const uint N = 100_000;
		var executions = N;
		var optimal = Exec(N, PlayOptimal);
		var random = Exec(N, PlayRandom);
		return (executions, optimal, random);
	}
}