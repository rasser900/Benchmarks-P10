﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class PrisonersBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Prisoners", "Benchmarking the original 100 Prisoners Benchmark")]
	public ulong PrisonersOriginal() {
		Prisoners prisoners = new Prisoners();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners", "Benchmarking the 100 Prisoners Benchmark using Parallel.For")]
	public ulong PrisonersParallelFor() {
		PrisonersParallelFor prisoners = new PrisonersParallelFor();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners", "Benchmarking the 100 Prisoners Benchmark without using LINQ")]
	public ulong PrisonersNoLINQ() {
		PrisonersNoLinq prisoners = new PrisonersNoLinq();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners", "Benchmarking the 100 Prisoners Benchmark using array instead of list")]
	public ulong PrisonersArray() {
		PrisonersArray prisoners = new PrisonersArray();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners",
		"Benchmarking the 100 Prisoners Benchmark using array instead of list and without using LINQ")]
	public ulong PrisonersArrayNoLINQ() {
		PrisonersArrayNoLinq prisoners = new PrisonersArrayNoLinq();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners", "Benchmarking the 100 Prisoners Benchmark using Parallel.For and without using LINQ")]
	public ulong PrisonersParallelForNoLINQ() {
		PrisonersParallelForNoLinq prisoners = new PrisonersParallelForNoLinq();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}
	
	[Benchmark("Prisoners", "Benchmarking the 100 Prisoners Benchmark using Parallel.For and attay")]
	public ulong PrisonersParallelForArray() {
		PrisonersParallelForArray prisoners = new PrisonersParallelForArray();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}

	[Benchmark("Prisoners",
		"Benchmarking the 100 Prisoners Benchmark using Parallel.For, array, and without using LINQ")]
	public ulong PrisonersParallelForArrayNoLINQ() {
		PrisonersParallelForArrayNoLinq prisoners = new PrisonersParallelForArrayNoLinq();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += prisoners.Run().Item1;
		}

		return result;
	}
}