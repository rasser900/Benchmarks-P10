﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class AlmostPrimeBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("AlmostPrime", "Benchmarking the original AlmostPrime Benchmark")]
	public long AlmostPrimeOriginal() {
		AlmostPrimeOriginal almostPrime = new AlmostPrimeOriginal();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime", "Benchmarking the AlmostPrime Benchmark with use of manual threading for concurrency")]
	public long AlmostPrimeManualThread() {
		AlmostPrimeManualThread almostPrime = new AlmostPrimeManualThread();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime", "Benchmarking the AlmostPrime Benchmark with use of array instead of list")]
	public long AlmostPrimeArray() {
		AlmostPrimeArray almostPrime = new AlmostPrimeArray();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime", "Benchmarking the AlmostPrime Benchmark with use of struct instead of a class")]
	public long AlmostPrimeStruct() {
		AlmostPrimeStruct almostPrime = new AlmostPrimeStruct();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime",
		"Benchmarking the AlmostPrime Benchmark with use of manual threading for concurrency and arrays instead of list")]
	public long AlmostPrimeManualThreadArray() {
		AlmostPrimeManualThreadArray almostPrime = new AlmostPrimeManualThreadArray();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime",
		"Benchmarking the AlmostPrime Benchmark with use of manual threading for concurrency and struct for objects")]
	public long AlmostPrimeManualThreadStruct() {
		AlmostPrimeManualThreadStruct almostPrime = new AlmostPrimeManualThreadStruct();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime",
		"Benchmarking the AlmostPrime Benchmark with use of array instead of list and struct instead of class")]
	public long AlmostPrimeArrayStruct() {
		AlmostPrimeArrayStruct almostPrime = new AlmostPrimeArrayStruct();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}

	[Benchmark("AlmostPrime",
		"Benchmarking the AlmostPrime Benchmark with use of manual threading for concurrency and arrays instead of list and struct instead of class")]
	public long AlmostPrimeManualThreadArrayStruct() {
		AlmostPrimeManualThreadArrayStruct almostPrime = new AlmostPrimeManualThreadArrayStruct();
		long result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += almostPrime.Run();
		}

		return result;
	}
}