﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class FourSquarePuzzleBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the original Four Squares Benchmark")]
	public ulong FourSquarePuzzleOriginal() {
		FourSquarePuzzleOriginal fourSquarePuzzleOriginal = new FourSquarePuzzleOriginal();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleOriginal.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using Parallel.For")]
	public ulong FourSquarePuzzleParallelFor() {
		FourSquarePuzzleParallelFor fourSquarePuzzleParallelFor = new FourSquarePuzzleParallelFor();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleParallelFor.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark without using LINQ")]
	public ulong FourSquarePuzzleNoLINQ() {
		FourSquarePuzzleNoLINQ fourSquarePuzzleNoLinq = new FourSquarePuzzleNoLINQ();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleNoLinq.Run();
		}

		return result;
	}

	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using switch")]
	public ulong FourSquarePuzzleSelection() {
		FourSquarePuzzleSelection fourSquarePuzzleSelection = new FourSquarePuzzleSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleSelection.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using Parallel.For without LINQ")]
	public ulong FourParallelForNoLINQ() {
		FourSquarePuzzleParallelForNoLINQ fourSquarePuzzleParallelForNoLinq = new FourSquarePuzzleParallelForNoLINQ();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleParallelForNoLinq.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using Parallel.For and switch")]
	public ulong FourSquarePuzzleParallelForSelection() {
		FourSquarePuzzleParallelForSelection fourSquarePuzzleParallelForSelection = new FourSquarePuzzleParallelForSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleParallelForSelection.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using switch without LINQ")]
	public ulong FourSquarePuzzleNoLINQSelection() {
		FourSquarePuzzleNoLINQSelection fourSquarePuzzleNoLinqSelection = new FourSquarePuzzleNoLINQSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleNoLinqSelection.Run();
		}

		return result;
	}
	
	[Benchmark("FourSquarePuzzle", "Benchmarking the Four Squares Benchmark using switch and Parallel.For without LINQ")]
	public ulong FourSquarePuzzleParallelForNoLINQSelection() {
		FourSquarePuzzleParallelForNoLINQSelection fourSquarePuzzleParallelForNoLinqSelection = new FourSquarePuzzleParallelForNoLINQSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += fourSquarePuzzleParallelForNoLinqSelection.Run();
		}

		return result;
	}

}