﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class CompareStringLengthBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("CompareStringLength", "Benchmarking the original Compare String Length Benchmark")]
	public ulong CompareStringLengthOriginal() {
		CompareStringLengthOriginal compareStringLengthOriginal = new CompareStringLengthOriginal();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthOriginal.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength", "Benchmarking the Compare String Length Benchmark using switch")]
	public ulong CompareStringLengthSelection() {
		CompareStringLengthSelection compareStringLengthSelection = new CompareStringLengthSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelection.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength", "Benchmarking the Compare String Length Benchmark using Parallel.For")]
	public ulong CompareStringLengthParallelFor1() {
		CompareStringLengthParallelFor1 compareStringLengthParallelFor = new CompareStringLengthParallelFor1();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthParallelFor.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength", "Benchmarking the Compare String Length Benchmark using Parallel.For")]
	public ulong CompareStringLengthParallelFor2() {
		CompareStringLengthParallelFor2 compareStringLengthParallelFor = new CompareStringLengthParallelFor2();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthParallelFor.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength", "Benchmarking the Compare String Length Benchmark using foreach")]
	public ulong CompareStringLengthForeach() {
		CompareStringLengthForeach compareStringLengthForeach = new CompareStringLengthForeach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthForeach.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength", "Benchmarking the Compare String Length Benchmark using selection and foreach")]
	public ulong CompareStringLengthSelectionForeach() {
		CompareStringLengthSelectionForeach compareStringLengthSelectionForeach =
			new CompareStringLengthSelectionForeach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelectionForeach.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using selection and Parallel.For")]
	public ulong CompareStringLengthSelectionParallelFor1() {
		CompareStringLengthSelectionParallelFor1 compareStringLengthSelectionParallelFor1 =
			new CompareStringLengthSelectionParallelFor1();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelectionParallelFor1.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using selection and Parallel.For")]
	public ulong CompareStringLengthSelectionParallelFor2() {
		CompareStringLengthSelectionParallelFor2 compareStringLengthSelectionParallelFor2 =
			new CompareStringLengthSelectionParallelFor2();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelectionParallelFor2.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using Parallel.For and foreach")]
	public ulong CompareStringLengthParallelFor1Foreach() {
		CompareStringLengthParallelFor1Foreach compareStringLengthParallelFor1Foreach =
			new CompareStringLengthParallelFor1Foreach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthParallelFor1Foreach.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using Parallel.For and foreach")]
	public ulong CompareStringLengthParallelFor2Foreach() {
		CompareStringLengthParallelFor2Foreach compareStringLengthParallelFor2Foreach =
			new CompareStringLengthParallelFor2Foreach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthParallelFor2Foreach.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using selection, Parallel.For and foreach")]
	public ulong CompareStringLengthSelectionParallelFor1Foreach() {
		CompareStringLengthSelectionParallelFor1Foreach compareStringLengthSelectionParallelFor1Foreach =
			new CompareStringLengthSelectionParallelFor1Foreach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelectionParallelFor1Foreach.Run();
		}

		return result;
	}

	[Benchmark("CompareStringLength",
		"Benchmarking the Compare String Length Benchmark using selection, Parallel.For and foreach")]
	public ulong CompareStringLengthSelectionParallelFor2Foreach() {
		CompareStringLengthSelectionParallelFor2Foreach compareStringLengthSelectionParallelFor2Foreach =
			new CompareStringLengthSelectionParallelFor2Foreach();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += compareStringLengthSelectionParallelFor2Foreach.Run();
		}

		return result;
	}
}