﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class ChebyshevBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("Chebyshev", "Benchmarking the original Chebyshev Benchmark")]
	public double ChebyshevOriginal() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevOriginal.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark concurrently")]
	public double ChebyshevConcurrent() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevConcurrent.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark array")]
	public double ChebyshevArray() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevArray.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark with uint instead of int")]
	public double ChebyshevDatatype() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevDatatype.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark concurrent and array")]
	public double ChebyshevConcurrentArray() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevConcurrentArray.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark concurrent and datatype changes")]
	public double ChebyshevConcurrentDatatype() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevConcurrentDatatype.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark with array and datatype changes")]
	public double ChebyshevArrayDatatype() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevArrayDatatype.Program.Run();
		}

		return result;
	}

	[Benchmark("Chebyshev", "Benchmarking the Chebyshev Benchmark with concurrent, array and datatype changes")]
	public double ChebyshevConcurrentArrayDatatype() {
		double result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += Chebyshev.ChebyshevConcurrentArrayDatatype.Program.Run();
		}

		return result;
	}
}