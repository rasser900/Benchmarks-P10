﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class NumbrixPuzzleBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;

	[Benchmark("NumbrixPuzzle", "Benchmarking the original Numbrix puzzle")]
	public ulong NumbrixPuzzleOriginal() {
		NumbrixPuzzleOriginal numbrixPuzzle = new NumbrixPuzzleOriginal();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle", "Benchmarking the Numbrix puzzle without using LINQ")]
	public ulong NumbrixPuzzleLinq() {
		NumbrixPuzzleLinq numbrixPuzzle = new NumbrixPuzzleLinq();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle", "Benchmarking the Numbrix puzzle using Parallel.For for concurrency")]
	public ulong NumbrixPuzzleParallelFor() {
		NumbrixPuzzleParallelFor numbrixPuzzle = new NumbrixPuzzleParallelFor();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle", "Benchmarking the Numbrix puzzle using using switch for Selection")]
	public ulong NumbrixPuzzleSelection() {
		NumbrixPuzzleSelection numbrixPuzzle = new NumbrixPuzzleSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle", "Benchmarking the Numbrix puzzle using Parallel.For without using LINQ")]
	public ulong NumbrixPuzzleLinqParallelFor() {
		NumbrixPuzzleLinqParallelFor numbrixPuzzle = new NumbrixPuzzleLinqParallelFor();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle", "Benchmarking the Numbrix puzzle with switch for Selection and without using LINQ")]
	public ulong NumbrixPuzzleLinqSelection() {
		NumbrixPuzzleLinqSelection numbrixPuzzle = new NumbrixPuzzleLinqSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle",
		"Benchmarking the Numbrix puzzle using Parallel.For for concurrency and switch for Selection")]
	public ulong NumbrixPuzzleParallelForSelection() {
		NumbrixPuzzleParallelForSelection numbrixPuzzle = new NumbrixPuzzleParallelForSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}

	[Benchmark("NumbrixPuzzle",
		"Benchmarking the Numbrix puzzle using Parallel.For and switch for Selection without using LINQ")]
	public ulong NumbrixPuzzleLinqParallelForSelection() {
		NumbrixPuzzleLinqParallelForSelection numbrixPuzzle = new NumbrixPuzzleLinqParallelForSelection();
		ulong result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += numbrixPuzzle.Run();
		}

		return result;
	}
}