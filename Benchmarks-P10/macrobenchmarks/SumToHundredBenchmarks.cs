﻿using System.Diagnostics.CodeAnalysis;
using CsharpRAPL.Benchmarking.Attributes;

namespace Benchmarks_P10.macrobenchmarks;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class SumToHundredBenchmarks {
	public static ulong Iterations;
	public static ulong LoopIterations;


	[Benchmark("SumToHundred", "Benchmarking the original Sum To Hundred Benchmark")]
	public int SumToHundredOriginal() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredOriginal.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred", "Benchmarking the Sum To Hundred Benchmark with no linq in a concurrent manner")]
	public int SumToHundredNoLinqConcurrent() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredNoLINQConcurrent.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred", "Benchmarking the Sum To Hundred Benchmark with switches instead of if's")]
	public int SumToHundredSelection() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredSelection.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred",
		"Benchmarking the Sum To Hundred Benchmark with a struct instead of a class for commonly used variables")]
	public int SumToHundredObjects() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredObjects.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred",
		"Benchmarking the Sum To Hundred Benchmark with no linq in a concurrent manner and switches instead of if's")]
	public int SumToHundredNoLinqConcurrentSelection() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredNoLINQConcurrentSelection.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred",
		"Benchmarking the Sum To Hundred Benchmark with no linq in a concurrent manner and a struct instead of a class for commonly used variables")]
	public int SumToHundredNoLinqConcurrentObjects() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredNoLINQConcurrentObjects.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred",
		"Benchmarking the Sum To Hundred Benchmark with switches instead of if's and a struct instead of a class for commonly used variables")]
	public int SumToHundredSelectionObjects() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredSelectionObjects.SumToHundred.Run();
		}

		return result;
	}

	[Benchmark("SumToHundred",
		"Benchmarking the Sum To Hundred Benchmark with no linq in a concurrent manner, switches instead of if's, and a struct instead of a class for commonly used variables")]
	public int SumToHundredNoLinqConcurrentSelectionObjects() {
		int result = 0;
		for (ulong i = 0; i < LoopIterations; i++) {
			result += SumToHundred.SumToHundredNoLINQConcurrentSelectionObjects.SumToHundred.Run();
		}

		return result;
	}
}